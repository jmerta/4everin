<?php declare(strict_types=1);

namespace App\Form;

use App\DTO\OrderDTO;
use App\Entity\Order\DeliveryType;
use App\Entity\Order\FontType;
use App\Entity\Order\PaymentType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderFormType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => OrderDTO::class,
        ]);
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add(
            'productId',
            HiddenType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'itemsCount',
            HiddenType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'packeteryBranchName',
            HiddenType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'packeteryBranchId',
            HiddenType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'file',
            FileType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'photo3D',
            CheckboxType::class,
            [
                'required' => false,
            ]
        );

        $builder->add(
            'withoutText',
            CheckboxType::class,
            [
                'required' => false,
            ]
        );

        $builder->add(
            'text',
            TextType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'fontId',
            EntityType::class,
            [
                'class' => FontType::class,
                'required' => false,
                'expanded' => true,
                'multiple' => false,
                'choice_label' => function (FontType $type) {
                    return $type->getImage();
                },
                'choice_value' => function (?FontType $entity) {
                    return $entity ? $entity->getId() : '';
                },
                'choice_attr' => function ($val, $key, $index) {
                    return ['class' => 'form-check-input'];
                },
            ]
        );

        $builder->add(
            'deliveryId',
            EntityType::class,
            [
                'class' => DeliveryType::class,
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'choice_value' => function (?DeliveryType $entity) {
                    return $entity ? $entity->getId() : '';
                },
                'choice_attr' => function (?DeliveryType $entity, $key, $index) {
                    if ($entity) {
                        return [
                            'price' => $entity->getPrice()
                        ];
                    }
                }
            ]
        );

        $builder->add(
            'paymentId',
            EntityType::class,
            [
                'class' => PaymentType::class,
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'choice_value' => function (?PaymentType $entity) {
                    return $entity ? $entity->getId() : '';
                },
                'choice_attr' => function (?PaymentType $entity, $key, $index) {
                    if ($entity) {
                        return [
                            'price' => $entity->getPrice()
                        ];
                    }
                }
            ]
        );

        $builder->add(
            'name',
            TextType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'phone',
            TextType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'email',
            TextType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'company',
            CheckboxType::class,
            [
                'required' => false,
            ]
        );

        $builder->add(
            'companyName',
            TextType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'ein',
            TextType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'vatNumber',
            TextType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'street',
            TextType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'street',
            TextType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'city',
            TextType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'postCode',
            TextType::class,
            [
                'required' => true
            ]
        );

        $builder->add(
            'billingAddress',
            CheckboxType::class,
            [
                'required' => false,
            ]
        );

        $builder->add(
            'billingStreet',
            TextType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'billingCity',
            TextType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'billingPostCode',
            TextType::class,
            [
                'required' => false
            ]
        );

        $builder->add(
            'note',
            TextareaType::class,
            [
                'required' => false,
            ]
        );

        $builder->add(
            'gdprAgree',
            CheckboxType::class,
            [
                'required' => true,
            ]
        );

        $builder->add(
            'businessCondAgree',
            CheckboxType::class,
            [
                'required' => true,
            ]
        );

        $builder->setAction('/objednavka/odeslat');
    }
}

<?php declare(strict_types=1);

namespace App\DTO;

use App\Entity\Order\DeliveryType;
use App\Entity\Order\PaymentType;
use Symfony\Component\Validator\Constraints as Assert;

class OrderDTO {

    /**
     * @var int
     * @Assert\NotBlank()
     */
    public $productId;

    /**
     * @var int
     */
    public $itemsCount = 1;

    /**
     * @Assert\File(maxSize="2M")
     * @Assert\NotBlank()
     */
    public $file = null;

    /**
     * @var bool
     */
    public $photo3D = false;

    /**
     * @var bool
     */
    public $withoutText = false;

    /**
     * @var string|null
     * @Assert\Length(max="45", maxMessage="Maximální počet znaků je 45.")
     */
    public $text = null;

    /**
     * @var int|null
     */
    public $fontId = null;

    /**
     * @var DeliveryType
     * @Assert\NotBlank()
     */
    public $deliveryId = null;

    /**
     * @var string|null
     */
    public $packeteryBranchName = null;

    /**
     * @var int|null
     */
    public $packeteryBranchId = null;

    /**
     * @var PaymentType
     * @Assert\NotBlank()
     */
    public $paymentId = null;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $name;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $phone;

    /**
     * @var string
     * @Assert\Email()
     */
    public $email;

    /**
     * @var bool
     */
    public $company = false;

    /**
     * @var string|null
     */
    public $companyName = null;

    /**
     * @var string|null
     */
    public $ein = null;

    /**
     * @var string|null
     */
    public $vatNumber = null;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $street;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $city;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    public $postCode;

    public $billingAddress = false;

    /**
     * @var string|null
     */
    public $billingStreet = null;

    /**
     * @var string|null
     */
    public $billingCity = null;

    /**
     * @var string|null
     */
    public $billingPostCode = null;

    /**
     * @var string|null
     */
    public $note;

    /**
     * @var bool
     * @Assert\IsTrue()
     */
    public $gdprAgree = false;

    /**
     * @var bool
     * @Assert\IsTrue()
     */
    public $businessCondAgree = false;

}

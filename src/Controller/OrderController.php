<?php declare(strict_types=1);

namespace App\Controller;

use App\DTO\OrderDTO;
use App\Entity\Order;
use App\Entity\Product;
use App\Form\OrderFormType;
use App\Service\OrderMailSender;
use App\Service\OrderProcessor;
use DateTime;
use DomainException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController {

    public static function getSubscribedServices(): array
    {
        return  array_merge(parent::getSubscribedServices(), [
            'order.processor' => OrderProcessor::class,
            'order.mailer' => OrderMailSender::class
        ]);
    }

    /**
     * @Route("/objednavka/{productId}", name="eshop", methods={"GET"})
     */
    public function orderAction($productId = null) {
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepository->findAll();
        $selectedProduct = $productId ? $this->findProduct($products, (int)$productId) : null;

        $dto = new OrderDTO();
        $dto->productId = (int)$productId;
        $form = $this->createForm(OrderFormType::class, $dto);

        return $this->render(
            'eshop.html.twig',
            [
                'form' => $form->createView(),
                'products' => $products,
                'title' => 'Objednávka',
                'selectedProduct' => $selectedProduct,
                'description' => 'Objednejte Vaše foto ve skle online',
                'keywords' => 'fotky ve skle, foto ve skle, dárek z fotek, 2D/3D fotografie ve skle'
            ]
        );
    }

    /**
     * @Route("/objednavka/odeslat", name="placeOrder", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function sendOrderAction(Request $request) {
        $form = $this->createForm(OrderFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $fileName = $this->processFile($form['file']->getData());
                $this->get('order.processor')->process($form->getData(), $fileName);

                return $this->redirectToRoute('confirmation');
            } catch (DomainException $e) {
                $this->addFlash('error', $e->getMessage());
            }

        }

        if (!$form->isValid()) {
            $str = '';
            foreach ($form->getErrors(true) as $error) {
                $str .= $error->getMessage();
            }

            $this->addFlash('error', 'Některé údaje nejsou vyplněné správně! '.$str);
        }

        $productId = (int)$form->getData()->productId;
        $productRepository = $this->getDoctrine()->getRepository(Product::class);
        $products = $productRepository->findAll();
        $selectedProduct = $productId ? $this->findProduct($products, $productId) : null;

        return $this->render('eshop.html.twig', [
            'products' => $products,
            'selectedProduct' => $selectedProduct,
            'form' => $form->createView(),
            'title' => 'Objednávka',
            'description' => 'Objednejte Vaše foto ve skle online',
            'keywords' => 'fotky ve skle, foto ve skle, dárek z fotek, 2D/3D fotografie ve skle'
        ]);
    }

    /**
     * @Route("/potvrzeni-objednavky", name="confirmation")
     */
    public function confirmationAction()
    {
        return $this->render('confirmation.html.twig');
    }


    /**
     * @Route("test-mail/{orderId}", name="mail_test")
     */
    public function testMailTemplate($orderId) {
        $orderRepository = $this->getDoctrine()->getRepository(Order::class);
        $order = $orderRepository->find($orderId);

        if ($order) {
            $this->get('order.processor')->sendMail($order);
            return $this->render('mail.html.twig', ['order' => $order]);
        }
    }

    /**
     * @Route("send-test-mail/", name="send_mail_test")
     */
    public function testSendEmail() {
        $this->get('order.mailer')->sendTestMail();
    }

    private function findProduct(array $products, int $productId) {
        foreach ($products as $product) {
            if ($product->getId() === $productId) {
                return $product;
            }
        }

        return null;
    }

    private function processFile($file) {
        $directory = $this->getParameter('client_files_dir');

        // compute a random name and try to guess the extension (more secure)
        $extension = $file->guessExtension();
        if (!$extension) {
            // extension cannot be guessed
            $extension = 'bin';
        }
        $date = new DateTime();
        $fileName = md5($file->getClientOriginalName().$date->getTimestamp()).'.'.$extension;

        $file->move($directory, $fileName);

        return $fileName;
    }
}

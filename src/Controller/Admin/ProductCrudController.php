<?php

namespace App\Controller\Admin;

use App\Admin\Fields\VichImageField;
use App\Entity\Product;
use App\Entity\Project;
use App\FormType\ProjectImageType;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProductCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Product::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Produkty')
            ->setEntityLabelInSingular('Produkt')
            ->setSearchFields(['name', 'description'])
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Základní údaje')->setIcon('list'),
            IdField::new('id')->hideOnForm(),
            TextField::new('name')->setLabel('Název'),
            TextField::new('size')->setLabel('Rozměr'),
            NumberField::new('price')->setLabel('Cena'),
            NumberField::new('discountPrice')->setLabel('Akční cena'),
            FormField::addPanel('3D')->setIcon('laser'),
            BooleanField::new('allowed3DLaser')->setLabel('3D'),
            NumberField::new('laser3DPrice')->setLabel('Cena 3D'),
            FormField::addPanel('Obrázky')->setIcon('camera'),
            VichImageField::new('sampleImageFile')
                ->onlyOnForms()
                ->setLabel('Náhledové foto'),
            VichImageField::new('realImageFile')->onlyOnForms()->setLabel('Reálné foto')

        ];
    }

}

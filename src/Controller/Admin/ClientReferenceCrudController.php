<?php declare(strict_types=1);

namespace App\Controller\Admin;

use App\Admin\Fields\VichImageField;
use App\Entity\ClientReference;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;

class ClientReferenceCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return ClientReference::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInPlural('Naši klienti')
            ->setEntityLabelInSingular('Klient')
            ->setSearchFields(['name', 'description'])
            ->setDefaultSort(['id' => 'DESC']);
    }

    public function configureFields(string $pageName): iterable {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('name')->setLabel('Název'),
            UrlField::new('link')->setLabel('Odkaz na web'),
            VichImageField::new('imageFile')->onlyOnForms()->setLabel('Logo')
        ];
    }
}

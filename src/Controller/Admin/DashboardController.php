<?php

namespace App\Controller\Admin;

use App\Entity\ClientReference;
use App\Entity\Order;
use App\Entity\Order\DeliveryType;
use App\Entity\Order\FontType;
use App\Entity\Order\PaymentType;
use App\Entity\Product;
use App\Entity\Reference;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController {

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response {
        // redirect to some CRUD controller
        $routeBuilder = $this->get(AdminUrlGenerator::class);
        $url = $routeBuilder->setAction(Action::INDEX)->setController(OrderCrudController::class)->generateUrl();
        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard {
        return Dashboard::new()
            ->setTitle('<img width="50%" src="https://files.matfix.cz/img/logo/logo.svg" />')
            // set this option if you prefer the page content to span the entire
            // browser width, instead of the default design which sets a max width
            ->renderContentMaximized();

    }

    public function configureMenuItems(): iterable {
        yield MenuItem::linktoDashboard('Domů', 'fa fa-home');
        yield MenuItem::subMenu('Objednávky', 'fa fa-newspaper')->setSubItems(
            [
                 MenuItem::linkToCrud('Čekající', 'fa fa-clock', Order::class)
                    ->setQueryParameter('filters[processed]', 0),
                 MenuItem::linkToCrud('Všechny', 'fa fa-newspaper', Order::class)
            ]
        );

        yield MenuItem::linkToCrud('Produkty', 'fa fa-box', Product::class);
        yield MenuItem::subMenu('Reference', 'fa fa-pencil')->setSubItems(
            [
                MenuItem::linkToCrud('Slider', 'fa fa-user', Reference::class),
                MenuItem::linkToCrud('Klienti', 'fa fa-city', ClientReference::class)
            ]
        );
        yield MenuItem::subMenu('Nastavení', 'fa fa-cog')->setSubItems(
            [
                MenuItem::linkToCrud('Doprava', 'fa fa-car', DeliveryType::class),
                MenuItem::linkToCrud('Platba', 'fa fa-money', PaymentType::class),
                MenuItem::linkToCrud('Fonty', 'fa fa-bold', FontType::class)
            ]
        );
        yield MenuItem::linkToLogout('Odhlásit', 'fa fa-sign-out');
    }
}

<?php declare(strict_types=1);

namespace App\Service;

use App\DTO\OrderDTO;
use App\Entity\Order;
use App\Entity\Order\Client;
use App\Entity\Order\Client\Address;
use App\Entity\Order\DeliveryType;
use App\Entity\Order\FontType;
use App\Entity\Order\PaymentType;
use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use DomainException;

class OrderProcessor {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var OrderMailSender
     */
    private $orderMailSender;


    public function __construct(
        EntityManagerInterface $entityManager,
        OrderMailSender $orderMailSender
    ) {
        $this->entityManager = $entityManager;
        $this->orderMailSender = $orderMailSender;
    }

    public function process(OrderDTO $orderDTO, string $fileName) {
        $order = new Order();
        $this->entityManager->persist($order);
        $this->mapDataToOrder($order, $orderDTO, $fileName);
        $this->entityManager->flush();
        try {
            $this->sendMail($order);
        } catch (DomainException $exception) {
            unset($exception);
        }
    }

    public function sendMail(Order $order) {
        $this->orderMailSender->send($order);
    }

    private function mapDataToOrder(Order $order, OrderDTO $orderDTO, string $fileName) {
        $order->addProduct($this->findProduct((int)$orderDTO->productId));
        $order->setItemsCount((int)$orderDTO->itemsCount);
        $order->setPhoto($fileName);
        $order->setPhoto3D($orderDTO->photo3D);
        $order->setWithText(!$orderDTO->withoutText);
        if (!$orderDTO->withoutText) {
            $order->setText($orderDTO->text);
            $order->setFont($orderDTO->fontId);
        }
        $order->setDelivery($orderDTO->deliveryId);

        if ($orderDTO->deliveryId->getId() == 1) {
            $order->setPacketeryBranchName($orderDTO->packeteryBranchName);
            $order->setPacketeryBranchId((int)$orderDTO->packeteryBranchId);
        }

        $order->setPayment($orderDTO->paymentId);
        $order->setClient($this->mapClient($orderDTO));
        $order->setNote($orderDTO->note);
        $order->updateTotalPrice();

    }

    private function mapClient(OrderDTO $orderDTO): Client {
        $client = new Client();
        $this->entityManager->persist($client);

        $client->setName($orderDTO->name);
        $client->setPhone($orderDTO->phone);
        $client->setEmail($orderDTO->email);
        if ($orderDTO->company) {
            $client->setEin($orderDTO->ein);
            $client->setVatNumber($orderDTO->vatNumber);
            $client->setCompanyName($orderDTO->companyName);
        }

        $address = $this->createAddress($orderDTO->street, $orderDTO->city, $orderDTO->postCode);
        $client->setAddress($address);

        if ($orderDTO->billingAddress) {
            $billingAddress = $this->createAddress($orderDTO->billingStreet, $orderDTO->billingCity, $orderDTO->billingPostCode);
            $client->setBillingAddress($billingAddress);
        }

        return $client;
    }

    private function createAddress(string $street, string $city, string $postCode): Address {
        $address = new Address();
        $this->entityManager->persist($address);

        $address->setStreet($street);
        $address->setCity($city);
        $address->setPostCode($postCode);

        return $address;
    }

    private function findProduct(int $productId): Product {
       return $this->entityManager->getRepository(Product::class)->find($productId);
    }

    private function findFontType(int $fontId): FontType {
        return $this->entityManager->getRepository(FontType::class)->find($fontId);
    }

    private function findDeliveryType(int $deliveryId): DeliveryType {
        return $this->entityManager->getRepository(DeliveryType::class)->find($deliveryId);
    }

    private function findPaymentType(int $paymentId): PaymentType {
        return $this->entityManager->getRepository(PaymentType::class)->find($paymentId);
    }
}

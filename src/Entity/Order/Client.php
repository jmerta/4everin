<?php declare(strict_types=1);

namespace App\Entity\Order;

use App\Entity\Order\Client\Address;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Client {

    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=false)
     */
    protected $email;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $companyName;

    /**
     * @var string|null
     * @ORM\Column(type="string", nullable=true)
     */
    protected $ein;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $vatNumber;

    /**
     * @var Address
     * @ORM\OneToOne(targetEntity="App\Entity\Order\Client\Address", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=false)
     */
    protected $address;

    /**
     * @var Address|null
     * @ORM\OneToOne(targetEntity="App\Entity\Order\Client\Address", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(name="billing_address_id", referencedColumnName="id", nullable=true)
     */
    protected $billingAddress;

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPhone(): string {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getCompanyName(): ?string {
        return $this->companyName;
    }

    /**
     * @param string|null $companyName
     */
    public function setCompanyName(?string $companyName): void {
        $this->companyName = $companyName;
    }

    /**
     * @return string|null
     */
    public function getEin(): ?string {
        return $this->ein;
    }

    /**
     * @param string|null $ein
     */
    public function setEin(?string $ein): void {
        $this->ein = $ein;
    }

    /**
     * @return string|null
     */
    public function getVatNumber(): ?string {
        return $this->vatNumber;
    }

    /**
     * @param string|null $vatNumber
     */
    public function setVatNumber(?string $vatNumber): void {
        $this->vatNumber = $vatNumber;
    }

    /**
     * @return Address
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * @param Address $address
     */
    public function setAddress(Address $address): void {
        $this->address = $address;
    }

    /**
     * @return Address|null
     */
    public function getBillingAddress(): ?Address {
        return $this->billingAddress;
    }

    /**
     * @param Address|null $billingAddress
     */
    public function setBillingAddress(?Address $billingAddress): void {
        $this->billingAddress = $billingAddress;
    }
}
